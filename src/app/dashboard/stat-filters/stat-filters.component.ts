import { Component, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.css']
})
export class StatFiltersComponent implements OnDestroy {

  formGroup: FormGroup;

  regions = ['North America', 'Europe', 'Asia', 'All'];

  private sub: Subscription;

  constructor(fb: FormBuilder) {
    this.formGroup = fb.group({
      region: ['All'],
      fromDate: [''],
      toDate: [''],
      ageGroup: ['']
    });


    this.sub = this.formGroup.valueChanges
      .pipe(
        tap(v => console.log(v))
      ).subscribe();
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}
