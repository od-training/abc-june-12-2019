import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Video } from './types';

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {
  videoData: Observable<Video[]>;

  constructor(http: HttpClient) {
    this.videoData = http.get<Video[]>('https://api.angularbootcamp.com/videos');
  }
}
