import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Video } from '../types';
import { VideoDataService } from '../video-data.service';

@Component({
  selector: 'app-video-dashboard',
  template: `
    <app-video-list [videos]="videos |async"
                    [selectedVideo]="selectedVideo"
                    (videoSelected)="selectedVideo = $event"></app-video-list>
    <app-video-player [video]="selectedVideo"></app-video-player>
    <app-stat-filters></app-stat-filters>
  `,
  styleUrls: ['./video-dashboard.component.css']
})
export class VideoDashboardComponent {

  videos: Observable<Video[]>;
  selectedVideo: Video;

  constructor(videoDataSvc: VideoDataService) {
    this.videos = videoDataSvc.videoData;
  }

}
